
<x-app-layout>
    
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Produtos') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    
                    @include('layouts.bootstrap')
                    <form action="/produtos/{{$produto->id}}" method="POST">
                        @csrf
                        @method('PUT')
                    
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Nome:</strong>
                                    <input type="text" name="nome" id="nome" class="form-control" placeholder="Descrição" value="{{$produto->nome}}">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Preço:</strong>
                                    <input type="text" name="preco" id="preco" class="form-control" placeholder="Preço" value="{{$produto->preco}}">
                                </div>
                                <br>
                            </div>
                           
                            <div class="d-flex justify-content-end col-xs-12 col-sm-12 col-md-12">
                                <button type="submit" class="btn btn-success m-2">Salvar</button>
                                <a  href="{{ route('produtos.index') }}" class="m-2 btn btn-light me-3">Voltar</a>
                            </div>
                        </div>
                        
                        </form>
                    </html>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>