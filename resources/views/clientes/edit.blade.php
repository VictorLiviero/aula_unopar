
<x-app-layout>
    
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Clientes') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    
                    @include('layouts.bootstrap')
                    <form action="/clientes/{{$cliente->id}}" method="POST">
                        @csrf
                        @method('PUT')
                    
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Nome:</strong>
                                    <input type="text" name="name" id="name" class="form-control" value="{{$cliente->name}}">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>CPF/ CNPJ:</strong>
                                    <input type="text" name="cpf_cnpj" id="cpf_cnpj" class="form-control cpfOuCnpj" value="{{$cliente->cpf_cnpj}}">
                                </div>
                                <br>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>CEP:</strong>
                                    <span class="text-danger">*</span>
                                    <input type="text" name="zip_code" class="form-control zip_code" required placeholder="Informe o CEP"
                                    value="{{ $cliente->zip_code }}">
                                </div>
                            </div>
                           
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Rua:</strong>
                                    <span class="text-danger">*</span>
                                    <input type="text" name="address" class="form-control address" required value="{{ $cliente->address }}">
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Número:</strong>
                                    <span class="text-danger">*</span>
                                    <input type="text" name="number" class="form-control" required value="{{ $cliente->number }}">
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Bairro:</strong>
                                    <span class="text-danger">*</span>
                                    <input type="text" name="district" class="form-control district" required value="{{ $cliente->district }}" >
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Complemento:</strong>
                                    <span class="text-danger">*</span>
                                    <input type="text" name="complement" class="form-control" required value="{{ $cliente->complement }}">
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Cidade:</strong>
                                    <span class="text-danger">*</span>
                                    <input type="text" name="city" class="form-control city" required value="{{ $cliente->city }}">
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Estado:</strong>
                                    <span class="text-danger">*</span>
                                    <input type="text" name="state" class="form-control state" required value="{{ $cliente->state }}">
                                </div>
                            </div>
                           
                            <div class="d-flex justify-content-end col-xs-12 col-sm-12 col-md-12">
                                <button type="submit" class="btn btn-success m-2">Salvar</button>
                                <a  href="{{ route('clientes.index') }}" class="m-2 btn btn-light me-3">Voltar</a>
                            </div>
                        </div>
                        
                        </form>
                    </html>
                </div>
            </div>
        </div>
    </div>

    <script>
        var options = {
        onKeyPress: function (cpf, ev, el, op) {
            var masks = ['000.000.000-000', '00.000.000/0000-00'];
            $('.cpfOuCnpj').mask((cpf.length > 14) ? masks[1] : masks[0], op);
        }
    }
    
    $('.cpfOuCnpj').length > 11 ? $('.cpfOuCnpj').mask('00.000.000/0000-00', options) : $('.cpfOuCnpj').mask('000.000.000-00#', options);
    </script>
</x-app-layout>