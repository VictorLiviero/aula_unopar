<x-app-layout>
{{-- header abaixo do menu --}}
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Clientes') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <!DOCTYPE html>
                    <html lang="en">

                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <title>Listagem de Clientes</title>
                    </head>
                                       
                    <body>
                        @include('layouts.bootstrap')
                        <h1>Criar Cliente</h1>

                        <form action="/clientes" method="POST">
                            @csrf

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>Nome:</strong>
                                        <span class="text-danger">*</span>
                                        <input type="text" name="name" id="nome" class="form-control" required placeholder="Nome Cliente">
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>CPF/ CNPJ:</strong>
                                        <span class="text-danger">*</span>
                                        <input type="text" name="cpf_cnpj" id="document" class="cpfOuCnpj form-control" required placeholder="CPF/ CNPJ">
                                    </div>
                                    <br>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>CEP:</strong>
                                        <span class="text-danger">*</span>
                                        <input type="text" name="zip_code" class="form-control zip_code" required placeholder="Informe o CEP">
                                    </div>
                                </div>
                               
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>Rua:</strong>
                                        <span class="text-danger">*</span>
                                        <input type="text" name="address" class="form-control address" required >
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>Número:</strong>
                                        <span class="text-danger">*</span>
                                        <input type="text" name="number" class="form-control" required >
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>Bairro:</strong>
                                        <span class="text-danger">*</span>
                                        <input type="text" name="district" class="form-control district" required >
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>Complemento:</strong>
                                        <span class="text-danger">*</span>
                                        <input type="text" name="complement" class="form-control" required >
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>Cidade:</strong>
                                        <span class="text-danger">*</span>
                                        <input type="text" name="city" class="form-control city" required >
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>Estado:</strong>
                                        <span class="text-danger">*</span>
                                        <input type="text" name="state" class="form-control state" required >
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>Tipo de Cliente:</strong>
                                        <select class="js-example-basic-single form-select form-select-solid" name="type_id">
                                            <option value="" selected hidden>Selecione um Tipo de cliente</option>
                                            @foreach ($customerType as $ct)
                                                <option value="{{ $ct->id }}" {{ (isset($cliente) && $ct->id == $cliente->type->id) || $ct->id == old('terminal_id') ? 'selected' : '' }}>
                                                    {{ $ct->description }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="d-flex justify-content-end col-xs-12 col-sm-12 col-md-12">
                                    <button type="submit" class="btn btn-success m-2">Salvar</button>
                                    <a  href="{{ route('clientes.index') }}" class="m-2 btn btn-light me-3">Cancelar</a>
                                </div>
                            </div>

                        </form>
                    </body>

                    </html>
                </div>
            </div>
        </div>
    </div>

<script>
    var options = {
    onKeyPress: function (cpf, ev, el, op) {
        var masks = ['000.000.000-000', '00.000.000/0000-00'];
        $('.cpfOuCnpj').mask((cpf.length > 14) ? masks[1] : masks[0], op);
    }
}

$('.cpfOuCnpj').length > 11 ? $('.cpfOuCnpj').mask('00.000.000/0000-00', options) : $('.cpfOuCnpj').mask('000.000.000-00#', options);

$(document).on('blur', '.zip_code', function (e){
    e.preventDefault();
    var cep = $(this).val();

    $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados)

    {
        if(!("erro" in dados)) {
            //Atualiza os campos com o valor da consulta;
            $(".address").val(dados.logradouro);
            $(".district").val(dados.bairro);
            $(".city").val(dados.localidade);
            $(".state").val(dados.uf);
            
        } else {
            $(".address").val('');
            $(".district").val('');
            $(".city").val('');
            $(".state").val('');
        }
    }
);


});
</script>
    

</x-app-layout>

