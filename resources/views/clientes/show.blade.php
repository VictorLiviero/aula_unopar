<x-app-layout>

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Detalhe Cliente') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <!DOCTYPE html>
                    <html lang="en">

                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <title>Listagem de Clientes</title>
                    </head>

                    <a class="d-flex justify-content-end" href="/Clientes/create">Criar Novo Cliente</a>

                    <body>
                        @include('layouts.bootstrap')
                        <h1>Detalhes do Cliente</h1>
                        <p>Nome: {{ $cliente->name}}</p>
                        <p>CPF/ CNPJ: {{ $cliente->cpf_cnpj}}</p>
                        
                        <div class="d-flex justify-content-end col-xs-12 col-sm-12 col-md-12">
                            <a class="m-2 btn btn-primary me-3" href="/Clientes/{{$cliente->id}}/edit">Editar</a>
                            <a  href="{{ route('clientes.index') }}" class="m-2 btn btn-light me-3">Voltar</a>
                        </div>
                        
                    </body>

                    </html>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
