<x-app-layout>
{{-- header abaixo do menu --}}
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tipo de Cliente') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <!DOCTYPE html>
                    <html lang="en">
                        @include('layouts.bootstrap')
                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <title>Listagem de Tipo de Cliente</title>
                    </head>
                                       
                    <body>
                        
                        <h1>Criar Teipo de cliente</h1>

                        <form action="/customer-type" method="POST">
                            @csrf

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>Descrição:</strong>
                                        <span class="text-danger">*</span>
                                        <input type="text" name="description" class="form-control" required placeholder="Descrição">
                                    </div>
                                </div>
                                                               
                                <div class="d-flex justify-content-end col-xs-12 col-sm-12 col-md-12">
                                    <button type="submit" class="btn btn-success m-2">Salvar</button>
                                    <a  href="{{ route('customer-type.index') }}" class="m-2 btn btn-light me-3">Cancelar</a>
                                </div>
                            </div>

                        </form>
                    </body>

                    </html>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

