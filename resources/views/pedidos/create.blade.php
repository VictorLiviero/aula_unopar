<x-app-layout>
    {{-- header abaixo do menu --}}
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Produtos') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <!DOCTYPE html>
                    <html lang="en">

                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <title>Listagem de Produtos</title>
                    </head>

                    <body>
                        @include('layouts.bootstrap')
                        <h1>Criar Pedido</h1>

                        <form action="{{ route('pedidos.store') }}" method="POST">
                            @csrf

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>Cliente:</strong>
                                        <select class="js-example-basic-single form-select form-select-solid" name="customer_id">
                                            <option value="" selected hidden>Selecione um cliente</option>
                                            @foreach ($customers as $customer)
                                            <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>Produtos:</strong>
                                        <div>
                                            @foreach($produtos as $produto)
                                            <div class="row mb-2">
                                                <div class="col-md-4">
                                                    <input type="checkbox" name="produtos[{{ $produto->id }}][id]" value="{{ $produto->id }}">
                                                    <span class="m-2">{{ $produto->nome }}</span>
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="number" class="form-control" name="produtos[{{ $produto->id }}][amount]" placeholder="Quantidade">
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex justify-content-end col-xs-12 col-sm-12 col-md-12">
                                <button type="submit" class="btn btn-success m-2">Salvar</button>
                                <a href="{{ route('pedidos.index') }}" class="m-2 btn btn-light me-3">Cancelar</a>
                            </div>
                        </form>
                    </body>

                    </html>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>