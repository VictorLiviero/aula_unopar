<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Editar Pedido') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    @include('layouts.bootstrap')
                    <h1>Editar Pedido</h1>

                    <form action="{{ route('pedidos.update', $pedido->id) }}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Cliente:</strong>
                                    <select class="js-example-basic-single form-select form-select-solid" name="customer_id" readonly>
                                        <option value="" selected hidden>Selecione um cliente</option>
                                        @foreach ($customers as $customer)
                                            <option value="{{ $customer->id }}" @if($pedido->customer_id == $customer->id) selected @endif>{{ $customer->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Produtos:</strong>
                                    <div>
                                        @foreach($produtos as $produto)
                                            <div class="row mb-2">
                                                <div class="col-md-4">
                                                    <input type="checkbox" name="produtos[{{ $produto->id }}][id]" value="{{ $produto->id }}" 
                                                    @if($pedido->items->contains('product_id', $produto->id)) checked @endif>
                                                    <span class="m-2">{{ $produto->nome }}</span>
                                                    <input type="hidden" name="produtos[{{ $produto->id }}][amount]" value="{{ $pedido->items->firstWhere('product_id', $produto->id)->amount ?? 0 }}">
                                                    <input type="hidden" name="produtos[{{ $produto->id }}][delete]" value="0">
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="number" class="form-control" name="produtos[{{ $produto->id }}][amount]" placeholder="Quantidade"
                                                    value="{{ $pedido->items->firstWhere('product_id', $produto->id)->amount ?? 0 }}">
                                                </div>
                                                <div class="col-md-4">
                                                    @if($pedido->items->contains('product_id', $produto->id))
                                                        <a href="#" class="btn btn-danger" onclick="deleteItem('{{ $pedido->items->firstWhere('product_id', $produto->id)->id }}')">Excluir</a>
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex justify-content-end col-xs-12 col-sm-12 col-md-12">
                            <button type="submit" class="btn btn-success m-2">Salvar</button>
                            <a href="{{ route('pedidos.index') }}" class="m-2 btn btn-light me-3">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        function deleteItem(itemId) {
            if (confirm('Tem certeza que deseja excluir este item?')) {
                fetch(`{{ url('order-items') }}/${itemId}`, {  // Correção na URL
                    method: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                    },
                })
                .then(response => {
                    // Recarrega a página após a exclusão bem-sucedida
                    location.reload();
                })
                .catch(error => {
                    console.error('Erro:', error);
                    alert('Erro ao excluir o item');
                });
            }
        }
    </script>
</x-app-layout>