<x-app-layout>
    
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Pedidos') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <!DOCTYPE html>
                    <html lang="en">
                        
                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <title>Listagem de Pedidos</title>
                    </head>
                    @include('layouts.bootstrap')
                    <div class="d-flex justify-content-end">
                        <a class="btn btn-success" href="/pedidos/create"><i class="fa-solid fa-plus"></i></a>
                    </div>                    
                    
                    <body>
                        <h1>Listagem de Pedidos</h1>
                        <table class="table table-bordered">
                            <tr>
                                <th>Id</th>
                                <th>Cliente</th>
                                <th>Total</th>
                                <th width="280px">Açoes</th>
                            </tr>
                            @foreach ($pedidos as $pedido)
                            <tr>
                                
                                <td>{{ $pedido->id }}</td>
                                <td>{{ $pedido->customer->name }}</td>
                                <td>{{ $pedido->total }}</td>
                                <td>
                                    <form action="{{ route('pedidos.destroy',$pedido->id) }}" method="POST">
                       
                                        <a class="btn btn-info" href="{{ route('pedidos.show', $pedido->id) }}"><i class="fa-solid fa-eye"></i></a>
                        
                                        <a class="btn btn-primary" href="{{ route('pedidos.edit',$pedido->id) }}"><i class="fa-solid fa-pen"></i></a>
                       
                                        @csrf
                                        @method('DELETE')
                          
                                        <button type="submit" class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </table>                       
                                            
                    </body>
                    </html>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>