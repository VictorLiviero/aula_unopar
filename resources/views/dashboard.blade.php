@php

use App\Models\Produto;
use App\Models\Customer;

$productsCount = Produto::count();
$customersCount = Customer::count(); 

@endphp
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="grid grid-cols-1 sm:grid-cols-2 gap-4">
                
                <!-- Card for Products -->
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 text-gray-900">
                        <h3 class="text-lg font-semibold">{{ __('Produtos') }}</h3>
                        <p class="mt-2 text-2xl">
                            {{ $productsCount }} <!-- Replace with actual count -->
                        </p>
                        <a href="{{ route('produtos.index') }}" class="mt-4 inline-block text-blue-500">
                            Visualizar Produtos
                        </a>
                    </div>
                </div>

                <!-- Card for Customers -->
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 text-gray-900">
                        <h3 class="text-lg font-semibold">{{ __('Clientes') }}</h3>
                        <p class="mt-2 text-2xl">
                            {{ $customersCount }} 
                        </p>
                        <a href="{{ route('clientes.index') }}" class="mt-4 inline-block text-blue-500">
                            Visualizar Clientes
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>