<?php

namespace App\Http\Controllers;

use App\Models\CustomerType;

use Illuminate\Http\Request;

class CustomerTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $customerType = CustomerType::all();
        return view('customer-type.index', compact('customerType'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('customer-type.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        CustomerType::create([
            'description' => $request->input('description'),

        ]);

        return redirect('/customer-type');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $customerType = CustomerType::find($id);
        return view('customer-type.show', compact('customerType'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $customerType = CustomerType::find($id);
        return view('customer-type.edit', compact('produto'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $produto = CustomerType::find($id);

        $produto->update([
            'description' => $request->input('description'),

        ]);

        return redirect('/customer-type');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $produto = CustomerType::find($id);
        $produto->delete();

        return redirect('/customer-type');
    }
}
