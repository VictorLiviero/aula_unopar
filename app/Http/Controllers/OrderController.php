<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderItems;
use App\Models\Produto;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Exibir todos os pedidos com informações de clientes e produtos associados.
     */
    public function index()
    {
        $pedidos = Order::with('customer', 'items.product')->get();
        return view('pedidos.index', compact('pedidos'));
    }

    /**
     * Exibir o form de criação de um novo pedido.
     */
    public function create()
    {
        $customers = Customer::all();
        $produtos = Produto::all();
        return view('pedidos.create', compact('customers', 'produtos'));
    }

    /**
     * Método responsável por salvar no banco de dados.
     */
    public function store(Request $request)
    {
        $pedido = Order::create([
            'customer_id' => $request->customer_id,
            'total' => 0,
        ]);

        $total = 0;
        // Iteração com os produtos vindos do form
        foreach ($request->produtos as $produto) {
            if (isset($produto['id'])) {
                $produtoModel = Produto::find($produto['id']);
                $price = $produtoModel->preco * $produto['amount'];

                // Criar um novo item de pedido com os detalhes do produto e quantidade
                OrderItems::create([
                    'order_id' => $pedido->id,
                    'product_id' => $produto['id'],
                    'amount' => $produto['amount'],
                    'price' => $price,
                ]);
                $total += $price;
            }
        }

        // Atualiza o total do pedido com o valor calculado 
        $pedido->update(['total' => $total]);

        return redirect()->route('pedidos.index');
    }

    /**
     * Método para exibir os detalhes de um pedido em especifico;
     */
    public function show(Order $pedido)
    {
        // Carrega os relacionamentos para o pedido
        $pedido->load('customer', 'items.product');
        return view('pedidos.show', compact('pedido'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Order $pedido)
    {
        // Obtemos todos os clientes e produtos para popular os campos do form.
        $customers = Customer::all();
        $produtos = Produto::all();

        //Carregar aqui os itens do pedido:
        $pedido->load('items');

        return view('pedidos.edit', compact('pedido', 'customers', 'produtos',));
    }

    /**
     * Método para atualizar um pedido existente no banco.
     */
    public function update(Request $request, Order $pedido)
    {
        // Atualizar o cliente associado ao pedido
        $pedido->update([
            'customer_id' => $request->customer_id,
        ]);

        $total = 0;

        // Laço sobre os itens do pedido;
        foreach ($pedido->items as $item) {
            //Verificar se o produto esta presente no form, e se quantidade for maior que zero;
            if (isset($request->produtos[$item->product_id]) && $request->produtos[$item->product_id]['amount'] > 0) {
                //Atualizar a quantidade do produto no pedido;
                $amount = $request->produtos[$item->product_id]['amount'];
                $item->update(['amount' => $amount]);

                // Calcular o preço atual do pedido;

                $price = $item->product->preco * $amount;
                $total += $price;
            } else {
                // Remover o item do pedido se nao estiver presente no form
                $item->delete();
            }
        }

        // Adicionar ao pedido novos itens;
        foreach ($request->produtos as $produtoId => $data) {
            if (!$pedido->items->contains('product_id', $produtoId) && $data['amount'] > 0) {
                $produtoModel = Produto::find($produtoId);
                $price = $produtoModel->preco * $data['amount'];

                //Criar um novo item de pedido
                $pedido->items()->create([
                    'product_id' => $produtoId,
                    'amount' => $data['amount'],
                    'price' => $price,
                ]);
                $total += $price;
            }
        }

        // Atualizar o total do pedido:
        $pedido->update(['total' => $total]);

        return redirect()->route('pedidos.index');
    }

    /**
     * Método para excluir um pedido do banco de dados.
     */
    public function destroy(Order $pedido)
    {
        $pedido->delete();
        return redirect()->route('pedidos.index');
    }

    // Método para excluir um item específico de um pedido
    public function destroyOrderItem($id)
    {
        $orderItem = OrderItems::find($id);

        if ($orderItem) {
            $orderItem->delete();
        }
        return redirect()->back();
    }
}
