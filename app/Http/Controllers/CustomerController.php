<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\CustomerType;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $clientes = Customer::all();
        return view('clientes.index', compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $customerType = CustomerType::all();
        return view('clientes.create', compact('customerType'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();
        Customer::create($input);

        return redirect('/clientes');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $cliente = Customer::find($id);
        return view('clientes.show', compact('cliente'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $cliente = Customer::find($id);
        return view('clientes.edit', compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $cliente = Customer::find($id);

        $cliente->update([
            'name' => $request->input('name'),
            'cpf_cnpj' => $request->input('cpf_cnpj'),
        ]);

        return redirect('/clientes');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $cliente = Customer::find($id);
        $cliente->delete();

        return redirect('/clientes');
    }
}
