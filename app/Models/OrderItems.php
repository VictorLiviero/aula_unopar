<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    use HasFactory;

    // Os campos que serão permitidos serem salvos no banco
    protected $fillable = [
        'order_id',
        'product_id',
        'amount',
        'price',
    ];

    // Relacionamento: Um item de pedido pertence a um pedido
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    // Relacionamento: Um item de pedido esta associado a um produto
    public function product()
    {
        return $this->belongsTo(Produto::class);
    }
}
