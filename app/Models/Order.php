<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    // Os campos que serão permitidos serem salvos no banco
    protected $fillable = [
        'customer_id',
        'total'
    ];

    // Relacionamento: Um pedido pertence a um cliente
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    // Relacionamento: Um pedido possui vários itens
    public function items()
    {
        return $this->hasMany(OrderItems::class);
    }
}
