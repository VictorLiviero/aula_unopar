<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'cpf_cnpj',
        'zip_code',
        'address',
        'number',
        'district',
        'complement',
        'city',
        'state',
        'type_id'
    ];

    public function type()
    {
        return $this->belongsTo(CustomerType::class, 'type_id', 'id');
    }
}
